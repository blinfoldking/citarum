import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Modal from 'react-modal'
import './appMhsInput.scss';

const modalStyle = {
    content: { 
        padding: '0',
        width: '500px',
        height: '100px',
        top: '50vh',
        left: '0',
        right: '0',
        margin: '0 auto'
    }
}
export default class AppMhsInput extends Component {

    constructor(props) {
        super(props)

        this.state = {
            modal: false,
            jobs: [{
                location: 'Sungai Citarum',
                job_desc: 'lorem ipsum ti amte consectur',
                kuota: 15
            }, {
                location: 'Sungai Citarum',
                job_desc: 'lorem ipsum ti amte consectur',
                kuota: 15
            }, {
                location: 'Sungai Citarum',
                job_desc: 'lorem ipsum ti amte consectur',
                kuota: 15
            }, {
                location: 'Sungai Citarum',
                job_desc: 'lorem ipsum ti amte consectur',
                kuota: 15
            }, {
                location: 'Sungai Citarum',
                job_desc: 'lorem ipsum ti amte consectur',
                kuota: 15
            }]
        }
    }


    selectJob(id) {
        this.setState(
            {
                selectedJob: this.state.jobs[id]
            }
        )
    }

    openModal() {
        this.setState({modal: true})
    }

    render() {

        if (this.state.selectedJob === undefined) {
    
                return (
                    <div className="app-mhs-input">
                        <strong>
                            Pilih Pekerjaan
                        </strong>
                        <input placeholder="Cari Pekerjaan"/>
                        <div className="job-container">                    
                        {this.state.jobs.map((e, i) => (
                                <div key={i} className="job-card">
                                    <div>
                                    <strong>Lokasi</strong>
                                    <span>{e.location}</span>
                                    <strong>Deskripsi Pekerjaan</strong>
                                    <span>{e.job_desc}</span>
                                    <strong>Kuota</strong>
                                    <span>{e.kuota}</span>
                                    </div>
                                    <button className="btn btn-success btn-bottom" onClick={this.selectJob.bind(this, i)}>Pilih</button>
                                </div>
                            )
                        )}
                        </div>
                    </div>
            )
        }

        return (
            <div className="information-form">
                <div>
                    <strong>Data Pribadi</strong><br/>
                    <div className="input-section">
                        <strong>Nama Lengkap</strong>
                        <input placeholder="Nama Lengkap"/><br/>
                    </div>
                    <div className="input-section">
                        <strong>NIM</strong>
                        <input placeholder="NIM"/><br/>
                    </div>
                    <div className="input-section">
                        <strong>Program Studi</strong>
                        <input placeholder="Program Studi"/><br/>
                    </div>
                    <div className="input-section">
                        <strong>No Telepon</strong>
                        <input placeholder="No Telp."/><br/>
                    </div>
                    <div className="input-section">
                        <strong>Alamat Email</strong>
                        <input placeholder="Alamat Email"/><br/>
                    </div>
                </div>
                <div className="input-section">
                    <strong>SKS</strong><br/>
                    <input placeholder="Jumlah SKS yang telah lulus"/><br/>
                </div>
                    <input placeholder="Jumlah SKS di semester berjalan"/><br/>
                <br/>
                <button onClick={this.openModal.bind(this)} className="btn btn-success">Selesai</button>
                <Modal
                    style={
                        modalStyle
                    }
                    isOpen={this.state.modal}>
                    <p className="content">
                        Data Mahasiswa berhasil terkirim ke belmawa
                    </p>
                    <Link to="/kema/app/home"><button className="btn btn-bottom btn-success">Oke</button></Link>
                </Modal>
            </div>
        )
    }
}