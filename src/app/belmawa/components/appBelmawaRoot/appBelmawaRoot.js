import React, {Component} from 'react';
import AppBelmawaSidebar from './appBelmawaSidebar/appBelmawaSidebar';
import AppBelmawaHome from '../screens/appBelmawaHome/appBelmawaHome';
import AppBelmawaStudents from '../screens/appBelmawaStudents/appBelmawaStudents'
// import AppBelmawaInput from '../screens/appBelmawaInput/appBelmawaInput'
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './appBelmawaRoot.scss';

export default class AppBelmawaRoot extends Component {

    
    constructor(props) {
        super(props);
        this.state = {
            screen: props.match.params.state
        }
        console.log(this.state.screen)
    }

    getState (screen) {
        // console.log(this.state)
        this.setState({screen})
    }

    render() {
        return (
            <Router>
                <div className="app-belmawa-root">
                    <AppBelmawaSidebar></AppBelmawaSidebar>
                    <div className="content">
                        <h2>Welcome To KKN Citarum</h2>
                        <div className="screen">
                            <Route path="/belmawa/app/home" component={AppBelmawaHome}></Route>
                            <Route path="/belmawa/app/students" component={AppBelmawaStudents}></Route>
                            {/* <Route path="/belmawa/app/input" component={AppBelmawaInput}></Route> */}
                        </div>
                    </div>
                </div>
            </Router>
        )
    }
}