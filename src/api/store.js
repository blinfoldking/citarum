import citarum from './citarum';
import {observable, action, computed} from 'mobx';

class state {
				@observable isLogin = false;
}

const store = new state();
export default store;
