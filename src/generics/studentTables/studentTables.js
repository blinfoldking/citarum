import React, {Component} from 'react';
import Modal from 'react-modal';
import './studentTables.scss'

const modalStyle = {
    content: {
        top: '20vh',
        padding: '0',
        bottom: '20vh',
        height: '700px',
        width: '700px',
        margin: '0 auto'
    }
}
export default class StudentTables extends Component {
    constructor(props) {
        super(props);

        this.state = {
            page: 1,
            maxPage: Math.ceil(this.props.students.length/6),
            modalIsOpen: false,
            detailedView: {}
        }
    }

    OpenModal(id) {
        console.log(id);
        this.setState({
            modalIsOpen: true,
            detailedView: this.props.students[id]
        })
    }

    CloseModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    NextPage() {
        this.setState({
            page: this.state.page + 1
        })
        console.log(this.state)
    }

    PrevPage() {
        this.setState({
            page: this.state.page - 1
        })
        console.log(this.state)
    }

    render() {
        return (
            <div>
                <strong>Daftar Mahasiswa</strong>
                <div className="filters">
                    <input placeholder="Cari Mahasiswa"/>
                    
                    <label>Filter :</label>
                    <select>
                        <option>
                        </option>
                    </select>
                </div>
                <div className="table-container">
                    <table>
                         <thead>           
                            <tr>     
                                <th>Waktu</th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>    
                            {this.props.students
                                .map((e, i) => {
                                    if (i < this.state.page * 6  && i > (this.state.page - 1) * 6)  
                                    return (
                                    <tr key={i}>
                                        <td>{e.time}</td>
                                        <td>{e.nim}</td>
                                        <td>{e.name}</td>
                                        <td>{e.status}</td>
                                        <td><a onClick={this.OpenModal.bind(this, i)}>Lihat Detail</a></td>
                                    </tr>
                            )})}
                        </tbody>
                    </table>
                    <nav>
              
                            {
                                this.state.page === 1 ?
                                <ul className="pagination">
                                        <li className="page-item disabled"><a className="page-link">Previous</a></li>
                                        <li className="page-item active"><a className="page-link">1</a></li>
                                        <li className="page-item"><a className="page-link">2</a></li>
                                        <li className="page-item"><a className="page-link">3</a></li>
                                        <li className="page-item"><a onClick={this.NextPage.bind(this)} className="page-link">Next</a></li>
                                    </ul>
                                : (this.state.page === this.state.maxPage ?
                                    <ul className="pagination">
                                        <li className="page-item"><a onClick={this.PrevPage.bind(this)} className="page-link">Previous</a></li>
                                        <li className="page-item"><a className="page-link">{this.state.page - 2}</a></li>
                                        <li className="page-item"><a className="page-link">{this.state.page - 1}</a></li>
                                        <li className="page-item active"><a className="page-link">{this.state.page}</a></li>
                                        <li className="page-item disabled"><a className="page-link">Next</a></li>
                                    </ul>
                                        : <ul className="pagination">
                                        <li className="page-item"><a onClick={this.PrevPage.bind(this)} className="page-link">Previous</a></li>
                                        <li className="page-item"><a className="page-link">{this.state.page - 1}</a></li>
                                        <li className="page-item active"><a className="page-link">{this.state.page}</a></li>
                                        <li className="page-item"><a className="page-link">{this.state.page + 1}</a></li>
                                        <li className="page-item"><a onClick={this.NextPage.bind(this)} className="page-link">Next</a></li>
                                    </ul>)
                            }
                        
                        </nav>
                    </div>
                <Modal 
                    style={modalStyle}
                    isOpen={this.state.modalIsOpen}
                    >
                    <div className="content">
                        <div className="section">
                                <strong>
                                    Data Pribadi
                                </strong>
                                <div className="subsection-container">
                                    <div className="subsection">
                                        <strong>Nama Lengkap</strong><br/>
                                        <span>{this.state.detailedView.name}</span>
                                    </div>
                                    <div className="subsection">
                                        <strong>Program Studi</strong><br/>
                                        <span>{this.state.detailedView.prodi}</span>
                                    </div>
                                    <div className="subsection">
                                        <strong>NIM</strong><br/>
                                        <span>{this.state.detailedView.nim}</span>
                                    </div>
                                    <div className="subsection">
                                        <strong>Alamat Email</strong><br/>
                                        <span>{this.state.detailedView.email}</span>
                                    </div>
                                </div>
                        </div>
                        <br/>
                        <div className="section">
                                <strong>
                                    SKS
                                </strong>
                                <div className="subsection">
                                    <strong>Jumlah SKS yang telah lulus</strong><br/>
                                    <span>{this.state.detailedView.totalsks}</span>
                                </div>
                                <div className="subsection">
                                    <strong>Jumlah SKS di semester berjalan</strong><br/>
                                    <span>{this.state.detailedView.currsks}</span>
                                </div>
                        </div>
                        <br/>
                        <div className="section">
                                <strong>
                                    Pekerjaan
                                </strong>
                                <div className="subsection">
                                    <strong>Lokasi</strong><br/>
                                    <span>{this.state.detailedView.location}</span>
                                </div>
                                <div className="subsection">
                                    <strong>Deskripsi Pekerjaan</strong><br/>
                                    <span>{this.state.detailedView.job_desc}</span>
                                </div>
                        </div>
                    </div>
                    
                    <button className="btn btn-success" onClick={this.CloseModal.bind(this)}>Kembali</button>
                    <button className="btn btn-success" onClick={this.CloseModal.bind(this)}>Lihat Nilai</button>
                </Modal>
            </div>
        )
    }
}