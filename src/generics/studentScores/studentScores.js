import React, {Component} from 'react';
import './studentScore.scss';

export default class Scores extends Component {
	render() {
		return (
			<div>
				<div className="student-data">
					<strong className="title">Data Mahasiswa</strong>
					<div className="section">
						<strong>
							Nama Lengkap
						</strong>
						<span>
							adhkwgialvfwoauvdfu
						</span>
					</div>
					<div className="section">
						<strong>
							Dosen Penilai
						</strong>
						<span>
							adhkwgialvfwoauvdfu
						</span>
					</div>
					<div className="section">
						<strong>
							NIM
						</strong>
						<span>
							adhkwgialvfwoauvdfu
						</span>
					</div>
					<div className="section">
						<strong>
							Mahasiswa Penilai
						</strong>
						<span>
							adhkwgialvfwoauvdfu
						</span>
					</div>
					<div className="section">
						<strong>
							Program Studi
						</strong>
						<span>
							adhkwgialvfwoauvdfu
						</span>
					</div>
				</div>
				<div className="scores-table">
					<table>
						<thead>
							<tr>
								<th>
									Kriteria Penilaian
								</th>
								<th>
									Penilaian Dosen
								</th>
								<th>
									Penilaian Mahasiswa
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		)
	}
}
