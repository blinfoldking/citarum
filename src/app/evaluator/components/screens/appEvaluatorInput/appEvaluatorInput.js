import React, {Component} from 'react';
import './appEvaluatorInput.scss'
export default class AppEvaluatorInput extends Component {
	render() {
		return (
			<div className="app-evaluator-input">
				<table className="table table-condensed">
					<tr>
						<th>Kriteria Penilaian</th>
						<th>Skala Penilaian</th>
					</tr>
					<tr>
						<td>
							<strong>Komunikasi</strong>
						</td>
						<td>
							<select>
							<option value="" disabled selected>Pilih Skala...</option>
								{Array(11).fill().map((e, i) => <option>{i}</option>)}
							</select>
						</td>
					</tr>
					
					<tr>
						<td>
							<strong>Toleransi</strong>
						</td>
						<td>
							<select>
							<option value="" disabled selected>Pilih Skala...</option>
								{Array(11).fill().map((e, i) => <option>{i}</option>)}
							</select>
						</td>
					</tr>
					
					<tr>
						<td>
							<strong>Organisasi</strong>
						</td>
						<td>
							<select>
							<option value="" disabled selected>Pilih Skala...</option>
								{Array(11).fill().map((e, i) => <option>{i}</option>)}
							</select>
						</td>
					</tr>
					
					<tr>
						<td>
							<strong>IT Literacy</strong>
						</td>
						<td>
							<select>
							<option value="" disabled selected>Pilih Skala...</option>
								{Array(11).fill().map((e, i) => <option>{i}</option>)}
							</select>
						</td>
					</tr>
					
					<tr>
						<td>
							<strong>Tanggung Jawab</strong>
						</td>
						<td>
							<select>
							<option value="" disabled selected>Pilih Skala...</option>
								{Array(11).fill().map((e, i) => <option>{i}</option>)}
							</select>
						</td>
					</tr>
					
					<tr>
						<td>
							<strong>Critical Thinking</strong>
						</td>
						<td>
							<select>
							<option value="" disabled selected>Pilih Skala...</option>
								{Array(11).fill().map((e, i) => <option>{i}</option>)}
							</select>
						</td>
					</tr>
					
					<tr>
						<td>
							<strong>Leadership</strong>
						</td>
						<td>
							<select>
							<option value="" disabled selected>Pilih Skala...</option>
								{Array(11).fill().map((e, i) => <option>{i}</option>)}
							</select>
						</td>
					</tr>
					
					<tr>
						<td>
							<strong>Percaya Diri</strong>
						</td>
						<td>
							<select>
							<option value="" disabled selected>Pilih Skala...</option>
								{Array(11).fill().map((e, i) => <option>{i}</option>)}
							</select>
						</td>
					</tr>
					
					<tr>
						<td>
							<strong>Inisiatif</strong>
						</td>
						<td>
							<select>
							<option value="" disabled selected>Pilih Skala...</option>
								{Array(11).fill().map((e, i) => <option>{i}</option>)}
							</select>
						</td>
					</tr>
					
					<tr>
						<td>
							<strong>Problem Based Learning</strong>
						</td>
						<td>
							<select>
							<option value="" disabled selected>Pilih Skala...</option>
								{Array(11).fill().map((e, i) => <option>{i}</option>)}
							</select>
						</td>
					</tr>
					

					<tr>
						<td>
							<button className="btn btn-success">Approve</button>
						</td>
						<td>
							<button className="btn btn-success">Simpan</button>
						</td>
					</tr>
				</table>
			</div>
		)
	}
}