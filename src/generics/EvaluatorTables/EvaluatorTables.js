import React, {Component} from 'react';
import Modal from 'react-modal';
import './EvaluatorTables.scss'

const modalStyle = {
    content: {
        top: '20vh',
        padding: '0',
        bottom: '20vh',
        height: '700px',
        width: '700px',
        margin: '0 auto'
    }
}
export default class EvaluatorTables extends Component {
    constructor(props) {
        super(props);

        this.state = {
            page: 1,
            maxPage: Math.ceil(this.props.evaluator.length/6),
            modalIsOpen: false,
            detailedView: {}
        }
    }

    OpenModal(id) {
        console.log(id);
        this.setState({
            modalIsOpen: true,
            detailedView: this.props.evaluator[id]
        })
    }

    CloseModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    NextPage() {
        this.setState({
            page: this.state.page + 1
        })
        console.log(this.state)
    }

    PrevPage() {
        this.setState({
            page: this.state.page - 1
        })
        console.log(this.state)
    }

    render() {
        return (
            <div>
                <strong>Daftar Evaluator</strong>
                    <div className="table-container">
                        <table>
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th>status</th>
                                    <th>Status Akun</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>    
                                {this.props.evaluator
                                    .map((e, i) => {
                                        if (i < this.state.page * 6  && i > (this.state.page - 1) * 6)  
                                        return (
                                        <tr key={i}>
                                            <td>{e.username}</td>
                                            <td>{e.name}</td>
                                            <td>{e.jobStatus}</td>
                                            <td>{e.status}</td>
                                            <td><a onClick={this.OpenModal.bind(this, i)}>Lihat Detail</a></td>

                                        </tr>
                                )})}
                            </tbody>
                        </table>
                        <nav>
              
                            {
                                this.state.page === 1 ?
                                <ul className="pagination">
                                        <li className="page-item disabled"><a className="page-link">Previous</a></li>
                                        <li className="page-item active"><a className="page-link">1</a></li>
                                        <li className="page-item"><a className="page-link">2</a></li>
                                        <li className="page-item"><a className="page-link">3</a></li>
                                        <li className="page-item"><a onClick={this.NextPage.bind(this)} className="page-link">Next</a></li>
                                    </ul>
                                : (this.state.page === this.state.maxPage ?
                                    <ul className="pagination">
                                        <li className="page-item"><a onClick={this.PrevPage.bind(this)} className="page-link">Previous</a></li>
                                        <li className="page-item"><a className="page-link">{this.state.page - 2}</a></li>
                                        <li className="page-item"><a className="page-link">{this.state.page - 1}</a></li>
                                        <li className="page-item active"><a className="page-link">{this.state.page}</a></li>
                                        <li className="page-item disabled"><a className="page-link">Next</a></li>
                                    </ul>
                                        : <ul className="pagination">
                                        <li className="page-item"><a onClick={this.PrevPage.bind(this)} className="page-link">Previous</a></li>
                                        <li className="page-item"><a className="page-link">{this.state.page - 1}</a></li>
                                        <li className="page-item active"><a className="page-link">{this.state.page}</a></li>
                                        <li className="page-item"><a className="page-link">{this.state.page + 1}</a></li>
                                        <li className="page-item"><a onClick={this.NextPage.bind(this)} className="page-link">Next</a></li>
                                    </ul>)
                            }
                        
                        </nav>
                    </div>
                <Modal 
                    style={modalStyle}
                    isOpen={this.state.modalIsOpen}
                    >
                    <div className="content">
                        <div className="section">
                                <strong>
                                    Data Akun
                                </strong>
                                <div className="subsection-container">
                                    <div className="subsection">
                                        <strong>Username</strong><br/>
                                        <span>{this.state.detailedView.username}</span>
                                    </div>
                                    <div className="subsection">
                                        <strong>Nama</strong><br/>
                                        <span>{this.state.detailedView.name}</span>
                                    </div>
                                    <div className="subsection">
                                        <strong>Password</strong><br/>
                                        <span>{this.state.detailedView.pass}</span>
                                    </div>
                                </div>
                        </div>
                        <br/>
                        <div className="section">
                                <strong>
                                    Status Akun
                                </strong>
                                <div className="subsection">
                                    <strong>Status</strong><br/>
                                    <span>{this.state.detailedView.jobStatus}</span>
                                </div>
                                <div className="subsection">
                                    <strong>Status Akun</strong><br/>
                                    <span>{this.state.detailedView.status}</span>
                                </div>
                        </div>
                        <br/>
                        {this.state.detailedView.student ?  
                            <div className="section">
                                <strong>
                                    Mahasiswa yang dinilai
                                </strong>
                                <div className="subsection">
                                    <strong>Nama Lengkap</strong>
                                    <span>{this.state.detailedView.student.name}</span>
                                </div>
                                <div className="subsection">
                                    <strong>NIM</strong>
                                    <span>{this.state.detailedView.student.nim}</span>
                                </div>
                                <div className="subsection">
                                    <strong>Program Studi</strong>
                                    <span>{this.state.detailedView.student.prodi}</span>
                                </div>
                            </div>
                            : ''
                        }
                        
                    </div>
                    
                    <button className="btn btn-success btn-bottom" onClick={this.CloseModal.bind(this)}>Kembali</button>
                </Modal>
            </div>
        )
    }
}