import React, {Component} from 'react';
import AppMhsSidebar from './appMhsSidebar/appMhsSidebar';
import AppMhsHome from '../screens/appMhsHome/appMhsHome';
import AppMhsStudents from '../screens/appMhsStudents/appMhsStudents'
import AppMhsInput from '../screens/appMhsInput/appMhsInput'
import AppMhsEvaluator from '../screens/appMhsEvaluator/appMhsEvaluator'
import AppMhsJobs from '../screens/appMhsJobs/appMhsJobs'
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './appMhsRoot.scss';

export default class AppMhsRoot extends Component {

    
    constructor(props) {
        super(props);
        this.state = {
            screen: props.match.params.state
        }
    }

    getState (screen) {
        // console.log(this.state)
        this.setState({screen})
    }

    render() {
        return (
            <Router>
                <div className="app-mhs-root">
                    <AppMhsSidebar></AppMhsSidebar>
                    <div className="content">
                        <h2>Welcome To KKN Citarum</h2>
                        <div className="screen">
                            <Route path="/kema/app/home" component={AppMhsHome}></Route>
                            <Route path="/kema/app/students" component={AppMhsStudents}></Route>
                            <Route path="/kema/app/input" component={AppMhsInput}></Route>
                            <Route path="/kema/app/evaluator" component={AppMhsEvaluator}></Route>
                            <Route path="/kema/app/jobs" component={AppMhsJobs}></Route>
                        </div>
                    </div>
                </div>
            </Router>
        )
    }
}
