import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Modal from 'react-modal'
import './appMhsInput.scss';

export default class AppMhsInput extends Component {

    constructor(props) {

        super(props)

        this.state = {
            jobs: [{
                location: 'Sungai Citarum',
                job_desc: 'lorem ipsum ti amte consectur',
                kuota: 15
            }, {
                location: 'Sungai Citarum',
                job_desc: 'lorem ipsum ti amte consectur',
                kuota: 15
            }, {
                location: 'Sungai Citarum',
                job_desc: 'lorem ipsum ti amte consectur',
                kuota: 15
            }, {
                location: 'Sungai Citarum',
                job_desc: 'lorem ipsum ti amte consectur',
                kuota: 15
            }, {
                location: 'Sungai Citarum',
                job_desc: 'lorem ipsum ti amte consectur',
                kuota: 15
            }]
        }
    }


    selectJob(id) {
        this.setState(
            {
                selectedJob: this.state.jobs[id]
            }
        )
    }

    render() {

        if (this.state.selectedJob === undefined) {
    
                return (
                    <div className="app-mhs-input">
                    <strong>
                        Pilih Pekerjaan
                    </strong>
                    <input placeholder="Cari Pekerjaan"/>
                    {this.state.jobs.map((e, i) => (
                        <div key={i} className="job-card">
                            <strong>Lokasi</strong>
                            <span>{e.location}</span>
                            <strong>Deskripsi Pekerjaan</strong>
                            <span>{e.job_desc}</span>
                            <strong>Kuota</strong>
                            <span>{e.kuota}</span>
                            <button className="btn btn-success" onClick={this.selectJob.bind(this, i)}>Pilih</button>
                        </div>
                    )
                )}
                </div>
            )
        }

        return (
            <div>
                <div>
                    <strong>Data Pribadi</strong><br/>
                    <input placeholder="Nama Lengkap"/><br/>
                    <input placeholder="NIM"/><br/>
                    <input placeholder="Program Studi"/><br/>
                    <input placeholder="No Telp."/><br/>
                    <input placeholder="Alamat Email"/><br/>
                </div>
                <div>
                    <strong>SKS</strong><br/>
                    <input placeholder="Jumlah SKS yang telah lulus"/><br/>
                    <input placeholder="Jumlah SKS di semester berjalan"/><br/>
                </div>
                <br/>
                <button className="btn btn-success">Selesai</button>
                <Modal
                isOpen={true}>
                    Data Mahasiswa berhasil terkirim ke belmawa<br/>
                    <Link to="/kema/app/home"><button className="btn btn-success">Oke</button></Link>
                </Modal>
            </div>
        )
    }
}