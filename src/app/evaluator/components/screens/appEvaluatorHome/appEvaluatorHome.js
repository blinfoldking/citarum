import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Card from '../../../../../generics/Card/Card';

import './appEvaluatorHome.scss'

export default class AppEvaluatorHome extends Component {
    render() {
        return (
            <div className="app-eval-home">
                <div className="input-container">
                    <Link to="/evaluator/app/input/">
                        <button className="btn">
                            Mulai Input Nilai
                        </button>
                    </Link>
                </div>
                <div>
                    <strong className="m-t">
                        Data Mahasiswa yang Dinilai
                    </strong>
                    <div className="i-c">
                        <div className="i-c-s">
                            <strong className="i-c-s-t">
                                Data Pribadi
                            </strong>
                            <strong>Nama Lengkap</strong>
                            <span>Lorem Ipsum</span>
                            <strong>NIM</strong>
                            <span>130xxxxxxxx</span>
                        </div>
                        <div className="i-c-s">
                            <strong className="i-c-s-t">
                                Pekerjaan
                            </strong>
                            <strong>Lokasi</strong>
                            <span>Sungai Citarum</span>
                            <strong>Deskripsi Pekerjaan</strong>
                            <span>lorem ipsum </span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}