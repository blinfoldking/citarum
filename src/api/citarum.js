import axios from 'axios';
import FormData from 'form-data';
import {action, computed, observable} from 'mobx';

class citarum {
	@observable isLogin = false;
	@observable token = '';
  @observable students = []
	constructor(url, client_secret = '', token = '') {
		this.d = { url, client_secret, token }
	}

	post(data) {
		axios({
			method: 'post',
			header: {
				Authentification: 'Bearer ${this.d.token}',
			},
			url: this.d.url,
			body: data
		});
	}

	get(route, cb) {
		console.log('kakak')
		axios({
			method: 'get',
			url: this.d.url + route,
			Authorization: 'Bearer ' + this.d.token
		})
		    .then(res => cb(res))
		    .catch(err => console.log(err))
	}

	toForm(obj) {
		let res = new FormData();

		for (let key in obj) {
			console.log(key);
			res.append(key, obj[key])
		}
		console.log(res);
		return res;
	}

	@action
	getJobs() {
		let res;
		this.get('/api/allJobs', response => res = response)
	}

	@action
	getStudents() {
		let res;
					this.get('api/allStudent', response => this.students = 'hallo')
					console.log(this.students)
	}

	@action
	login(username, password) {
		axios({
			method: 'post',
			url: this.d.url + '/oauth/token',
			data: this.toForm({
				'username': username,
				'password': password,
				'client_id': 2,
				'client_secret': this.d.client_secret,
				'scope': '',
				'grant_type': 'password',
			}),
			config:
			    {header: {'Content-Type': 'multipart/form-data'}}
		})
		    .then(res => {
			    this.d.token = res.data.access_token;
			    if (res.status == 200) this.isLogin = true;
		    })
		    .catch(err => console.log(err));
	}

	@action
	logout() {
		this.d.token = '';
		this.isLogin = false;
	}


	getStudents() {}
}

let api = new citarum(
    'https://televicient.tk', 'LUCCGeZvsmaBJuTC3SzliojNZtwew18oASDYOBPa');

export default api;
