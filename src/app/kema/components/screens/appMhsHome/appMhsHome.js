import React, {Component} from 'react';
import Card from '../../../../../generics/Card/Card'

import './appMhsHome.scss'

export default class AppMhsHome extends Component {
    render() {
        return (
            <div className="app-mhs-home">
                <Card
                    icon="fa fa-users"
                    title="Mahasiswa Masuk"
                    count="90"/>
                <Card
                    icon="fa fa-check"
                    title="Mahasiswa Approved"
                    count="90"/>
                <Card
                    icon="fa fa-times-circle"
                    title="Mahasiswa Declined"
                    count="90"/>
                <Card
                    icon="fa fa-hourglass"
                    title="Mahasiswa Pending"
                    count="90"/>
            </div>
        )
    }
}