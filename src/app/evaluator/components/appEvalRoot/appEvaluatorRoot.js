import React, {Component} from 'react';
import AppEvaluatorSidebar from './appEvalSidebar/appEvalSidebar'
import AppEvaluatorHome from '../screens/appEvaluatorHome/appEvaluatorHome';
import AppEvaluatorInput from '../screens/appEvaluatorInput/appEvaluatorInput';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './appEvaluatorRoot.scss';

export default class AppEvaluatorRoot extends Component {

    
    constructor(props) {
        super(props);
        this.state = {
            screen: props.match.params.state
        }
        console.log(this.state.screen)
    }

    getState (screen) {
        // console.log(this.state)
        this.setState({screen})
    }

    render() {
        return (
            <Router>
                <div className="app-evaluator-root">
                    <AppEvaluatorSidebar></AppEvaluatorSidebar>
                    <div className="content">
                        <h2>Welcome To KKN Citarum</h2>
                        <div className="screen">
                        </div>
                            <Route path="/evaluator/app/home" component={AppEvaluatorHome}></Route>
                            <Route path="/evaluator/app/input" component={AppEvaluatorInput}></Route>
                    </div>
                </div>
            </Router>
        )
    }
}