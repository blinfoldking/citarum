import React, {Component} from 'react';
import './appMhsJobs.scss'

export default class AppMhsJobs extends Component {
	
	constructor(props) {
		super(props)
		this.state = {
			jobs: [
				{
					loc: 'Sungai Citarum',
					desc: 'Membersihkan sungai citarum',
					kuota: 10
				}, {
					loc: 'Sungai Citarum',
					desc: 'Membersihkan sungai citarum',
					kuota: 15
				}
			]
		}
	}
	
	render() {
		return (
			<div className="app-mhs-jobs">
				<h3>Daftar Pekerjaan</h3>
				<input type="text" placeholder="Cari Pekerjaan"/>
				<div className="jobs-container">
					{
						this.state.jobs
							.map((job, i) => 
							<div
								key={i} 
								className="job-item">
								<table>
									<tr>
										<td>
											Lokasi
										</td>
										<td>
											{job.loc}
										</td>
									</tr>
									<tr>
										<td>
											Deskripsi Pekerjaan
										</td>
										<td>
											{job.desc}
										</td>
									</tr>
									<tr>
										<td>
											Kuota
										</td>
										<td>
											{job.kuota} orang lagi
										</td>
									</tr>
								</table>
							</div>
							)
					}
					<button className="btn btn-success">Load More</button>
				</div>
			</div>
		)
	}
}