import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './appEvalSidebar.scss';

export default class AppEvalSidebar extends Component {
    render() {
        return (
            <div className="app-mhs-sidebar">
                <div className="app-mhs-profile">
                    <div>
                        <i className="fa fa-user"></i>
                        <img/>
                    </div>
                    <div>
                        <h5>
                            Lorem Ipsum
                        </h5>
                    </div>
                </div>

                <div className="nav-list">
                    <div>
                        <h4>
                            <Link to="/evaluator/app/home">Beranda</Link>
                        </h4>
                    </div>
                    <div>
                        <h4>
                            <Link to="/evaluator/app/changepass">Ubah Password</Link>
                        </h4>
                    </div>
                    <div>
                        <h4>
                            <Link to="/evaluator/login">Keluar</Link>
                        </h4>
                    </div>
                </div>

            </div>
        )
    }
}