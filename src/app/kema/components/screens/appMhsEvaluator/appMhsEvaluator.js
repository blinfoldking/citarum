import React, {Component} from 'react'
import EvaluatorTables from '../../../../../generics/EvaluatorTables/EvaluatorTables'

export default class AppMhsEvaluator extends Component {
    render() {
        return (
            <div>
                <button className="btn btn-success">Tambah Evaluator</button>
                <EvaluatorTables
                    evaluator={
                        [
                            {
                                name: 'lorem ipsum ti amet',
                                username: 'loremIpsum',
                                pass: '1234',
                                jobStatus: 'Mahasiswa',
                                status: 'locked',
                                student: {
                                    name: 'Ganesha Danu',
                                    nim : '1300xxxx',
                                    prodi: 'S1 Teknik Informatika'
                                }
                            }, {
                                name: 'lorem ipsum ti amet',
                                username: 'loremIpsum',
                                pass: '1234',
                                jobStatus: 'Mahasiswa',
                                status: 'locked',
                                student: {
                                    name: 'Ganesha Danu',
                                    nim : '1300xxxx',
                                    prodi: 'S1 Teknik Informatika'
                                }
                            }, {
                                name: 'lorem ipsum ti amet',
                                username: 'loremIpsum',
                                pass: '1234',
                                jobStatus: 'Mahasiswa',
                                status: 'locked',
                                student: {
                                    name: 'Ganesha Danu',
                                    nim : '1300xxxx',
                                    prodi: 'S1 Teknik Informatika'
                                }
                            }, {
                                name: 'lorem ipsum ti amet',
                                username: 'loremIpsum',
                                pass: '1234',
                                jobStatus: 'Mahasiswa',
                                status: 'locked',
                                student: {
                                    name: 'Ganesha Danu',
                                    nim : '1300xxxx',
                                    prodi: 'S1 Teknik Informatika'
                                }
                            },
                        ]
                    }
                ></EvaluatorTables>
            </div>
        )
    }
}