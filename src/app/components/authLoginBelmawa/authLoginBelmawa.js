import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import './authLoginBelmawa.scss'

export default class AuthLoginBelmawa extends Component {
    render() {
        return (
            <div>
                <h1 className="welcome">
                    Selamat Datang di Aplikasi KKN Belmawa
                </h1>

                <div className="login-container">
                    <input type="text" placeholder="Username"></input>
                    <input type="password" placeholder="Password"></input>
                    <a href="">
                        <small>Lupa Password?</small>
                    </a>
                    <Link to="/belmawa/app/home">
                        <button className="btn btn-success">LOGIN</button>
                    </Link>
                </div>

                

             </div>
        )
        }
}