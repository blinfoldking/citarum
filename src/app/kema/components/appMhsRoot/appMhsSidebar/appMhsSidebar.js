import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import history from '../../../../../history'
import './appMhsSidebar.scss';

@inject('api')
@observer
export default class AppMhsSidebar extends Component {
				logout() {
								this.props.logout();
								history.push('kema/login');
								window.location.reload(true)
				}
    render() {
        return (
            <div className="app-mhs-sidebar">
                <div className="app-mhs-profile">
                    <div>
                        <i className="fa fa-user"></i>
                        <img/>
                    </div>
                    <div>
                        <h5>
                            Universitas Telkom
                        </h5>
                        
                        <strong>
                            Bandung
                        </strong>
                    </div>
                    
                    <Link to="/kema/app/input">
                        <button className="btn">
                            Input Mahasiswa
                        </button>
                    </Link>
                </div>

                <div className="nav-list">
                    <div>
                        <h4>
                            <Link to="/kema/app/home">Beranda</Link>
                        </h4>
                    </div>
                    <div>
                        <h4>
                            <Link to="/kema/app/jobs">Daftar Pekerjaan</Link>
                        </h4>
                    </div>
                    <div>
                        <h4>
                            <Link to="/kema/app/students">Daftar Mahasiswa</Link>
                        </h4>
                    </div>
                    <div>
                        <h4>
                            <Link to="/kema/app/evaluator">Evaluator</Link>
                        </h4>
                    </div>
                    <div>
                        <h4>
                            <Link to="/kema/app/changepass">Ubah Password</Link>
                        </h4>
                    </div>
                    <div>
                        <h4>
																<Link onClick={this.logout.bind(this)} to="/kema/login">Keluar</Link>
                        </h4>
                    </div>
                </div>

            </div>
        )
    }
}
