import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './appBelmawaSidebar.scss';

export default class AppBelmawaSidebar extends Component {
    render() {
        return (
            <div className="app-mhs-sidebar">
                <div className="app-mhs-profile">
                    <div>
                        <i className="fa fa-user"></i>
                        <img/>
                    </div>
                    <div>
                        <h5>
                            Belmawa
                        </h5>
                    </div>
                    
                    {/* <Link to="/belmawa/app/input"> */}
                        <button className="btn">
                            Input Pekerjaan
                        </button>
                    {/* </Link> */}
                </div>

                <div className="nav-list">
                    <div>
                        <h4>
                            <Link to="/belmawa/app/home">Beranda</Link>
                        </h4>
                    </div>
                    <div>
                        <h4>
                            <Link to="/belmawa/app/jobs">Daftar Pekerjaan</Link>
                        </h4>
                    </div>
                    <div>
                        <h4>
                            <Link to="/belmawa/app/students">Daftar Mahasiswa</Link>
                        </h4>
                    </div>
                    <div>
                        <h4>
                            <Link to="/belmawa/app/changepass">Ubah Password</Link>
                        </h4>
                    </div>
                    <div>
                        <h4>
                            <Link to="/belmawa/login">Keluar</Link>
                        </h4>
                    </div>
                </div>

            </div>
        )
    }
}