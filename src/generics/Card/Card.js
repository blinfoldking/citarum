import React, {Component} from 'react';
import './Card.scss'

export default class Card extends Component {
    render() {
        return (
            <div className="Card">
                <div className="icon">
                    <i className={this.props.icon}></i> 
                </div>
                <div className="desc">
                    <h6>{this.props.title}</h6>
                    <h1>{this.props.count}</h1>
                </div>
            </div>
        )    
    }
}