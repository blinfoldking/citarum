import './App.css';
import React, {Component} from 'react';
import {Router, Route, Redirect } from 'react-router-dom';
import history from './history';
import api from './api/citarum';
import AppBelmawaRoot from './app/belmawa/components/appBelmawaRoot/appBelmawaRoot';
import AuthLoginBelmawa from './app/components/authLoginBelmawa/authLoginBelmawa';
import AuthLoginEvaluator from './app/components/authLoginEval/authLoginEval';
import AuthLoginMhs from './app/components/authLoginMhs/authLoginMhs';
import AuthRegisterMhs from './app/components/authRegisterMhs/authRegisterMhs';
import AppEvaluatorRoot from './app/evaluator/components/appEvalRoot/appEvaluatorRoot';
import AppMhsRoot from './app/kema/components/appMhsRoot/appMhsRoot';
import {inject, observer} from 'mobx-react';
import logo from './logo.svg';




@inject('api')
@observer
class App extends Component {

	constructor(props) {
					super(props);
					console.log(history);
	}

	render() {
						return (
										<Router history={history}>
								<div className='App'>
												<Route exact path='/kema/login' render={() => (
																	!this.props.api.isLogin ?
																				<AuthLoginMhs/>
																	:
																				<Redirect to="/kema/app/home"/>
													)}	/>
												<Route exact path='/kema/register' component={
																AuthRegisterMhs}/>      
												<Route exact path='/belmawa/login' component={AuthLoginBelmawa} />
												<Route exact path='/evaluator/login' component={AuthLoginEvaluator} />
												<Route exact path='/kema/app/:state' component={AppMhsRoot} />
												<Route exact path='/belmawa/app/:state' component={
																AppBelmawaRoot} />
												<Route exact path='/evaluator/app/:state' component={
																AppEvaluatorRoot} />
								</div>
							</Router>
						);
				
  }
}

export default App;
